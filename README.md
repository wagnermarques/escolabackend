# SISTEMA ETEC DE CATRACA
 
Este é o repositório para trabalharmos o sistema de catraca da ETEC Zona Leste. Nele construiremos o sistema Backend com Orientação a Objetos e usando o modo padrão de trabalhar com uma branch para cada feauture.
 
## Instalação
 
Use a seguinte sequência de comandos para trabalhar no projeto.
 
```bash
git clone https://gitlab.com/wagnermarques/escolabackend.git
cd escolabackend/
git branch -M sua-branch-escolhida
```
 
## Como funcionará?
Cada aluno escolherá uma branch para trabalhar em cima e desenvolver uma função. Sugestão: anotar cada pessoa que irá trabalhar em um branch e combinar o que cada um vai fazer.
 
## Contribuição
 
Toda as contribuições serão feitas pelos alunos do 3DS do ano de 2024.
 
## Licença
 
[MIT]()